from dataclasses import dataclass


@dataclass
class RegisterEncryptInputEntity:
    username: str
    password: str
    first_name: str
    last_name: str


@dataclass
class RegisterInputEntity:
    username: str
    password: str
    first_name: str
    last_name: str
