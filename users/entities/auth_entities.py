from dataclasses import dataclass


@dataclass
class PublicKeyResponseEntity:
    public_key: str


@dataclass
class LoginInputEntity:
    username: str
    password: str
