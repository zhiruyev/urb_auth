from rest_framework.routers import DefaultRouter

from users.views.register_views import RegistrationViewSet
from users.views.users_views import AuthViewSet

app_name = "users"

router = DefaultRouter()
router.register("", AuthViewSet, basename="users")
router.register("", RegistrationViewSet, basename="registration")
urlpatterns = router.get_urls()
