from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError

from users.handlers.auth_handler import AuthHandler
from users.serializers.auth_serializers import (
    LoginInputSerializer,
    LoginResponseSerializer,
    PublicKeyResponseSerializer,
)
from users.serializers.token_serializers import CustomTokenObtainPairSerializer


class AuthViewSet(viewsets.GenericViewSet, generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(
        operation_summary="Получение публичного ключа",
        responses={"200": PublicKeyResponseSerializer},
    )
    @action(methods=["get"], detail=False)
    def public_key(self, request):
        response_entity = AuthHandler().get_public_key()

        return Response(
            PublicKeyResponseSerializer(response_entity).data, status=status.HTTP_200_OK
        )

    def get_authenticate_header(self, request):
        return "Bearer realm=api"

    def _decrypt(self, request):
        serializer = LoginInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()
        decrypted_request_data = AuthHandler().login(input_entity)

        return decrypted_request_data

    @swagger_auto_schema(
        operation_summary="Авторизация клиента",
        request_body=LoginInputSerializer,
        responses={"200": LoginResponseSerializer},
    )
    @action(methods=["post"], detail=False)
    def login(self, request):
        decrypted_request_data = self._decrypt(request)
        serializer = CustomTokenObtainPairSerializer(
            data=decrypted_request_data, context={"request": request}
        )
        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return Response(serializer.validated_data, status=status.HTTP_200_OK)
