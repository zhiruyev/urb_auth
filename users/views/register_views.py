from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from libs.authorization import UrbRegBasicAuthorization
from users.handlers.registration_handler import RegistrationHandler
from users.serializers.registration_serializers import (
    RegisterEncryptInputSerializer,
    RegisterInputSerializer,
    RegisterResponseSerializer,
)


class RegistrationViewSet(viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = (UrbRegBasicAuthorization,)

    @staticmethod
    def _decrypt(request):
        serializer = RegisterEncryptInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()
        decrypted_request_data = RegistrationHandler().decrypt_register(input_entity)

        return decrypted_request_data

    @swagger_auto_schema(
        operation_summary="Регистрация пользователя с шифрованием",
        request_body=RegisterEncryptInputSerializer,
        responses={"201": RegisterResponseSerializer},
    )
    @action(methods=["post"], detail=False)
    def register_encrypt(self, request, *args, **kwargs):
        decrypted_request_data = self._decrypt(request)
        serializer = RegisterInputSerializer(data=decrypted_request_data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        user = RegistrationHandler().register(input_entity)

        return Response(
            RegisterResponseSerializer(user).data, status=status.HTTP_201_CREATED
        )
