from rest_framework import exceptions, status


class IncorrectMessageException(exceptions.APIException):
    def __init__(self, detail=None):
        detail = f"incorrectly encrypted message; {detail or ''}"
        super().__init__(detail, None)

    status_code = status.HTTP_400_BAD_REQUEST
    default_code = "IncorrectMessageException"


class EncryptionErrorException(exceptions.APIException):
    def __init__(self, detail=""):
        super().__init__(detail, None)

    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_code = "EncryptionErrorException"


class DecryptionErrorException(exceptions.APIException):
    def __init__(self, detail=""):
        super().__init__(detail, None)

    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_code = "DecryptionErrorException"
