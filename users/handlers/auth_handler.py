from django.conf import settings

from users.entities.auth_entities import (
    LoginInputEntity,
    PublicKeyResponseEntity,
)
from libs.encryption import RSAEncryption


class AuthHandler:
    def __init__(self):
        self.encryption = RSAEncryption()

    def get_public_key(self):
        public_key = settings.PUBLIC_KEY
        return PublicKeyResponseEntity(public_key=public_key)

    def login(self, input_entity: LoginInputEntity):
        private_key = settings.PRIVATE_KEY
        username = self.encryption.decrypt(private_key, input_entity.username)
        password = self.encryption.decrypt(private_key, input_entity.password)
        return {"username": username, "password": password}
