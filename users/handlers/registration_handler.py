from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.exceptions import PermissionDenied

from libs.encryption import RSAEncryption
from users.entities.registration_entities import (
    RegisterEncryptInputEntity,
    RegisterInputEntity,
)

User = get_user_model()


class RegistrationHandler:
    def register(self, input_entity: RegisterInputEntity) -> User:
        if User.objects.filter(username=input_entity.username).exists():
            raise PermissionDenied("User with this username exists")

        user = User(
            username=input_entity.username,
            email=input_entity.username,
            first_name=input_entity.first_name,
            last_name=input_entity.last_name,
        )
        user.set_password(input_entity.password)
        user.save()
        return user

    def decrypt_register(self, input_entity: RegisterEncryptInputEntity):
        private_key = settings.PRIVATE_KEY
        username = RSAEncryption().decrypt(private_key, input_entity.username)
        password = RSAEncryption().decrypt(private_key, input_entity.password)

        return {
            "username": username,
            "password": password,
            "first_name": input_entity.first_name,
            "last_name": input_entity.last_name,
        }
