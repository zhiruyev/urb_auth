from rest_framework import serializers

from libs.serializers import BaseSerializer
from users.entities.registration_entities import (
    RegisterEncryptInputEntity,
    RegisterInputEntity,
)


class RegisterEncryptInputSerializer(BaseSerializer):
    username = serializers.CharField(max_length=1000)
    password = serializers.CharField(max_length=1000)
    first_name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)

    def create(self, validated_data: dict) -> RegisterEncryptInputEntity:
        return RegisterEncryptInputEntity(**validated_data)


class RegisterInputSerializer(BaseSerializer):
    username = serializers.CharField(max_length=200)
    password = serializers.CharField(min_length=8, max_length=30, required=True)
    first_name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)

    def create(self, validated_data: dict) -> RegisterInputEntity:
        return RegisterInputEntity(**validated_data)


class RegisterResponseSerializer(BaseSerializer):
    user_id = serializers.IntegerField(source="id")
