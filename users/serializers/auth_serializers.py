from rest_framework import serializers

from users.entities.auth_entities import LoginInputEntity
from users.entities.registration_entities import RegisterEncryptInputEntity
from libs.serializers import BaseSerializer


class PublicKeyResponseSerializer(BaseSerializer):
    public_key = serializers.CharField(max_length=1000)


class LoginInputSerializer(BaseSerializer):
    username = serializers.CharField(max_length=1000)
    password = serializers.CharField(max_length=1000)

    def create(self, validated_data: dict) -> LoginInputEntity:
        return LoginInputEntity(**validated_data)


class LoginResponseSerializer(BaseSerializer):
    refresh = serializers.CharField(max_length=1000)
    access = serializers.CharField(max_length=1000)


class RegisterEncryptInputSerializer(BaseSerializer):
    username = serializers.CharField(max_length=1000)
    password = serializers.CharField(max_length=1000)

    def create(self, validated_data: dict) -> RegisterEncryptInputEntity:
        return RegisterEncryptInputEntity(**validated_data)
