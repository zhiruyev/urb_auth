import uuid

import structlog

logger = structlog.getLogger('urb_auth')


def get_request_header(request, header_key, meta_key):
    if hasattr(request, 'headers'):
        return request.headers.get(header_key)

    return request.META.get(meta_key)


request_middleware_exclusions = [
    'admin',
    'api/swagger',
]


def _should_be_excluded(path: str):

    for exclusion in request_middleware_exclusions:
        if exclusion in path:
            return True

    return False


class LoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if _should_be_excluded(request.path):
            return self.get_response(request)

        request_id = get_request_header(
            request, 'x-request-id', 'HTTP_X_REQUEST_ID'
        ) or str(uuid.uuid4())

        correlation_id = get_request_header(
            request, 'x-correlation-id', 'HTTP_X_CORRELATION_ID'
        )

        with structlog.threadlocal.tmp_bind(logger):
            logger.bind(request_id=request_id)
            self.bind_user_id(request),
            if correlation_id:
                logger.bind(correlation_id=correlation_id)

            logger.info(
                'request_started',
                request=request,
                user_agent=request.META.get('HTTP_USER_AGENT'),
                body=request.body,
            )
            self._raised_exception = False
            response = self.get_response(request)
            if not self._raised_exception:
                self.bind_user_id(request),
                logger.info(
                    'request_finished', code=response.status_code, request=request, body=response.content,
                )

        return response

    @staticmethod
    def bind_user_id(request):
        if hasattr(request, 'user') and request.user is not None:
            user_id = request.user.pk
            if isinstance(user_id, uuid.UUID):
                user_id = str(user_id)
            logger.bind(user_id=user_id)
