import base64
import os
from typing import Optional

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

from users.constants import (
    ENCODING_TYPE,
    ENCRYPTION_KEY_BITS,
    PRIVATE_SUFFIX,
    PUBLIC_SUFFIX,
    SENTINEL_BITS,
)
from users.exceptions import (
    DecryptionErrorException,
    EncryptionErrorException,
    IncorrectMessageException,
)


class RSAEncryption:
    def __init__(self):
        self.bits = ENCRYPTION_KEY_BITS

    def _helper(self, key: bytes) -> str:
        return "\\n".join(key.decode(ENCODING_TYPE).splitlines())

    def generate_rsa_keys(self, prefix: Optional[str] = None):
        key_pair = RSA.generate(self.bits)
        public_key = self._helper(key_pair.public_key().export_key())
        private_key = self._helper(key_pair.export_key())
        if prefix:
            os.environ[PUBLIC_SUFFIX] = public_key
            os.environ[PRIVATE_SUFFIX] = private_key
            # for local testing :)
            with open(".env", mode="a") as f:
                keys = (
                    f"\n{PUBLIC_SUFFIX}={public_key}"
                    f"\n{PRIVATE_SUFFIX}={private_key}\n"
                )
                f.write(keys)

        return public_key, private_key

    @staticmethod
    def _cipher(key: bytes):
        key = RSA.import_key(key)
        return PKCS1_v1_5.new(key)

    def encrypt(self, public_key: str, message: str) -> str:
        public_key = public_key.encode(ENCODING_TYPE)
        encryptor = self._cipher(public_key)
        try:
            encrypted = base64.b64encode(
                encryptor.encrypt(message.encode(ENCODING_TYPE))
            )
            encrypted = encrypted.decode(ENCODING_TYPE)
        except Exception as e:
            raise EncryptionErrorException(detail=str(e))
        else:
            return encrypted

    def decrypt(self, private_key: str, message: str) -> str:
        private_key = private_key.encode(ENCODING_TYPE)
        decryptor = self._cipher(private_key)
        sentinel = get_random_bytes(SENTINEL_BITS)
        try:
            decrypted = decryptor.decrypt(
                ciphertext=base64.b64decode(message.encode(ENCODING_TYPE)),
                sentinel=sentinel,
            )
            if decrypted == sentinel:
                raise IncorrectMessageException()
            decrypted = decrypted.decode(ENCODING_TYPE)
        except Exception as e:
            raise DecryptionErrorException(detail=str(e))
        else:
            return decrypted
